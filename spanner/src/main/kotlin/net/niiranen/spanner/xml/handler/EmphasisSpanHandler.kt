package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder

import net.niiranen.spanner.FontFamily
import net.niiranen.spanner.SpanHandler
import net.niiranen.spanner.span.EmphasisSpan
import net.niiranen.spanner.span.FontFamilySpan.FontFamilySpanMark

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.countParentSpans
import net.niiranen.spanner.util.SpanUtil.getLastSpan
import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class EmphasisSpanHandler : SpanHandler<Attributes> {
    class EmphasisSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, EmphasisSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        val parents = countParentSpans(text, EmphasisSpanMark::class.java)

        val fontFamily: FontFamily
        val fontSpan = getLastSpan(text, FontFamilySpanMark::class.java)
        if (fontSpan != null) {
            fontFamily = fontSpan.fontFamily
        } else {
            fontFamily = FontFamily.defaultFontFamily
        }

        replaceLastMark(text, EmphasisSpanMark::class.java, EmphasisSpan(fontFamily, parents))
    }
}