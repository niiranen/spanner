package net.niiranen.spanner.xml.handler

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class StrongSpanHandler : SpanHandler<Attributes> {
    class StrongSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, StrongSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, StrongSpanMark::class.java, StyleSpan(Typeface.BOLD))
    }
}
