package net.niiranen.spanner.span

import net.niiranen.spanner.FontFamily

interface FontFamilyContainingSpan {
    val fontFamily: FontFamily
}
