package net.niiranen.spanner

import android.util.JsonReader
import android.util.JsonWriter

import java.io.IOException

class FontInfo {
    var name: String? = null
    var style: String? = null
    var weight: Int = 0
    var filename: String? = null
    var postScriptName: String? = null
    var fullName: String? = null
    var copyright: String? = null

    companion object {

        @Throws(IOException::class)
        fun fromJson(reader: JsonReader): FontInfo {
            val fontInfo = FontInfo()
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                if (name.equals("name", ignoreCase = true)) {
                    fontInfo.name = reader.nextString()
                } else if (name.equals("style", ignoreCase = true)) {
                    fontInfo.style = reader.nextString()
                } else if (name.equals("weight", ignoreCase = true)) {
                    fontInfo.weight = reader.nextInt()
                } else if (name.equals("filename", ignoreCase = true)) {
                    fontInfo.filename = reader.nextString()
                } else if (name.equals("postScriptName", ignoreCase = true)) {
                    fontInfo.postScriptName = reader.nextString()
                } else if (name.equals("fullName", ignoreCase = true)) {
                    fontInfo.fullName = reader.nextString()
                } else if (name.equals("copyright", ignoreCase = true)) {
                    fontInfo.copyright = reader.nextString()
                } else {
                    reader.skipValue()
                }
            }
            reader.endObject()
            return fontInfo
        }

        @Throws(IOException::class)
        fun toJson(fontInfo: FontInfo, writer: JsonWriter) {
            writer.beginObject()
                    .name("name").value(fontInfo.name)
                    .name("style").value(fontInfo.style)
                    .name("weight").value(fontInfo.weight.toLong())
                    .name("filename").value(fontInfo.filename)
                    .name("postScriptName").value(fontInfo.postScriptName)
                    .name("fullName").value(fontInfo.fullName)
                    .name("copyright").value(fontInfo.copyright)
                    .endObject()
        }
    }
}
