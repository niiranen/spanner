package net.niiranen.spanner

import android.util.JsonReader
import android.util.JsonWriter

import java.io.IOException
import java.util.ArrayList

class FontFamilyMetadata {
    var name: String? = null
    var designer: String? = null
    var license: String? = null
    var visibility: String? = null
    var category: String? = null
    var size: Long = 0
    var fontInfos: List<FontInfo>? = null
    var subsets: List<String>? = null

    companion object {

        @Throws(IOException::class)
        fun fromJson(reader: JsonReader): FontFamilyMetadata {
            val metadata = FontFamilyMetadata()
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                if (name.equals("name", ignoreCase = true)) {
                    metadata.name = reader.nextString()
                } else if (name.equals("designer", ignoreCase = true)) {
                    metadata.designer = reader.nextString()
                } else if (name.equals("license", ignoreCase = true)) {
                    metadata.license = reader.nextString()
                } else if (name.equals("category", ignoreCase = true)) {
                    metadata.category = reader.nextString()
                } else if (name.equals("visibility", ignoreCase = true)) {
                    metadata.visibility = reader.nextString()
                } else if (name.equals("size", ignoreCase = true)) {
                    metadata.size = reader.nextLong()
                } else if (name.equals("fonts", ignoreCase = true)) {
                    reader.beginArray()
                    val fontInfos = ArrayList<FontInfo>()
                    while (reader.hasNext()) {
                        fontInfos.add(FontInfo.fromJson(reader))
                    }
                    reader.endArray()
                    metadata.fontInfos = fontInfos
                } else if (name.equals("subsets", ignoreCase = true)) {
                    reader.beginArray()
                    val subsets = ArrayList<String>()
                    while (reader.hasNext()) {
                        subsets.add(reader.nextString())
                    }
                    reader.endArray()
                    metadata.subsets = subsets
                } else {
                    reader.skipValue()
                }
            }
            reader.endObject()
            return metadata
        }

        @Throws(IOException::class)
        fun toJson(
            metadata: FontFamilyMetadata,
            writer: JsonWriter
        ) {
            writer.beginObject()
                    .name("name").value(metadata.name)
                    .name("designer").value(metadata.designer)
                    .name("license").value(metadata.designer)
                    .name("visibility").value(metadata.visibility)
                    .name("category").value(metadata.category)
                    .name("size").value(metadata.size)
            writer.name("fonts").beginArray()
            for (fontInfo in metadata.fontInfos!!) {
                FontInfo.toJson(fontInfo, writer)
            }
            writer.endArray()
            writer.name("subsets").beginArray()
            for (subset in metadata.subsets!!) {
                writer.value(subset)
            }
            writer.endArray()
            writer.endObject()
        }
    }
}
