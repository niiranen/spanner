package net.niiranen.spanner.span

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.annotation.ColorInt
import android.text.Layout
import android.text.Spanned
import android.text.style.LeadingMarginSpan
import android.text.style.ParagraphStyle

class BlockQuoteSpan : LeadingMarginSpan, ParagraphStyle {

    override fun getLeadingMargin(first: Boolean): Int {
        return LEADING_MARGIN
    }

    override fun drawLeadingMargin(
        c: Canvas,
        p: Paint,
        x: Int,
        dir: Int,
        top: Int,
        baseline: Int,
        bottom: Int,
        text: CharSequence,
        start: Int,
        end: Int,
        first: Boolean,
        layout: Layout
    ) {
        // If its the first line, draw a quote mark
        if ((text as Spanned).getSpanStart(this) == start) {
            val startQuote = if (dir >= 0) LEFT_QUOTE else RIGHT_QUOTE
            val style = p.style
            @ColorInt val color = p.color
            val size = p.textSize
            p.style = Paint.Style.FILL
            p.color = COLOR
            p.textSize = QUOTE_SIZE.toFloat()

            c.drawText(startQuote, x + dir * (LEADING_MARGIN.toFloat() - p.measureText(startQuote) - GAP_WIDTH.toFloat()), baseline.toFloat(), p)

            p.textSize = size
            p.color = color
            p.style = style
        }
    }

    companion object {
        private val LEADING_MARGIN = 50
        private val GAP_WIDTH = 8
        private val QUOTE_SIZE = 80
        private val LEFT_QUOTE = "\u201C" // “
        private val RIGHT_QUOTE = "\u201D" // ”
        @ColorInt
        private val COLOR = Color.GRAY
    }
}
