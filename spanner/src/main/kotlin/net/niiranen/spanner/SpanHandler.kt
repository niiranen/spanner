package net.niiranen.spanner

import android.text.SpannableStringBuilder

/**
 * Interface for span handlers.
 *
 * @param <SpanDataType>
 * Type of data that should be sent to [.startSpan]
</SpanDataType> */
interface SpanHandler<SpanDataType> {
    /**
     * Start a span in text.
     *
     * @param text
     * Builder to start a span in.
     * @param spanData
     * Data about the span.
     */
    fun startSpan(text: SpannableStringBuilder, spanData: SpanDataType)

    /**
     * End a span in text.
     *
     * @param text
     * Builder to end a span in.
     */
    fun endSpan(text: SpannableStringBuilder)
}
