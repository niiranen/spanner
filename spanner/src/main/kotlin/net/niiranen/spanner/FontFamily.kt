package net.niiranen.spanner

import android.graphics.Typeface
import android.support.annotation.IntDef
import android.support.annotation.IntRange
import android.util.SparseArray
import net.niiranen.spanner.TypefaceWrapper.Companion.ITALIC
import net.niiranen.spanner.TypefaceWrapper.Companion.NORMAL

import net.niiranen.spanner.TypefaceWrapper.Style

import net.niiranen.spanner.util.SparseArrayUtil.getClosestValue
import kotlin.annotation.Retention

class FontFamily(@SerifStyle serifStyle: Int = UNKNOWN, @Proportion proportion: Int = PROPORTIONAL) {

    /**
     * SparseArray containing all the families typefaces.
     *
     *
     * The first key is the [Style] of the typeface, the second key is the weight of the
     * typeface.
     */
    private val mTypefaces = SparseArray<SparseArray<TypefaceWrapper>>(2)

    /**
     * Get the serif style of the family.
     */
    @SerifStyle
    val serifStyle: Int = serifStyle
    /**
     * Get the proportion of the family.
     */
    @Proportion
    val proportion: Int = proportion

    /**
     * Get the default typeface wrapper for this family.
     */
    /**
     * Set the default typeface wrapper for this family.
     */
    var defaultTypefaceWrapper = TypefaceWrapper(Typeface.DEFAULT)
    /**
     * Get the monospace typeface wrapper for this family.
     */
    /**
     * Set the monospace typeface wrapper to use.
     */
    var monospaceTypefaceWrapper = TypefaceWrapper(Typeface.MONOSPACE)
    /**
     * Get the sans serif typeface wrapper.
     */
    /**
     * Set the sans serif typeface wrapper to use.
     */
    var sansSerifTypefaceWrapper = TypefaceWrapper(Typeface.SANS_SERIF)
    /**
     * Get the serif typeface wrapper.
     */
    /**
     * Set the serif typeface wrapper to use.
     */
    var serifTypefaceWrapper = TypefaceWrapper(Typeface.SERIF)

    /**
     * Get the default typeface for this family.
     */
    /**
     * Set the default typeface for this family.
     */
    var defaultTypeface: Typeface
        get() = defaultTypefaceWrapper.typeface
        set(typeface) {
            defaultTypefaceWrapper = TypefaceWrapper(typeface)
        }

    /**
     * Get the monospace typeface for this family.
     */
    /**
     * Set the monospace typeface for this family.
     */
    var monospaceTypeface: Typeface
        get() = monospaceTypefaceWrapper.typeface
        set(typeface) {
            monospaceTypefaceWrapper = TypefaceWrapper(typeface)
        }

    /**
     * Get the sans serif typeface.
     */
    /**
     * Set the sans serif typeface to use.
     */
    var sansSerifTypeface: Typeface
        get() = sansSerifTypefaceWrapper.typeface
        set(sansSerifTypeface) {
            sansSerifTypefaceWrapper = TypefaceWrapper(sansSerifTypeface)
        }

    /**
     * Get the serif typeface.
     */
    /**
     * Set the serif typeface to use.
     */
    var serifTypeface: Typeface
        get() = serifTypefaceWrapper.typeface
        set(serifTypeface) {
            serifTypefaceWrapper = TypefaceWrapper(serifTypeface)
        }

    /**
     * Serif styles.
     */
    @Retention(AnnotationRetention.SOURCE)
    @IntDef(UNKNOWN, SANS_SERIF, SERIF)
    annotation class SerifStyle

    /**
     * Possible proportions.
     */
    @Retention(AnnotationRetention.SOURCE)
    @IntDef(PROPORTIONAL, MONOSPACE)
    annotation class Proportion

    fun addTypeface(typeface: Typeface) {
        addTypeface(TypefaceWrapper(typeface))
    }

    fun addTypeface(typeface: TypefaceWrapper) {
        var faces: SparseArray<TypefaceWrapper>? = mTypefaces.get(typeface.style)
        if (faces == null) {
            faces = SparseArray(1)
            faces.put(typeface.weight, typeface)
            mTypefaces.put(typeface.style, faces)
            return
        }
        faces.put(typeface.weight, typeface)
    }

    /**
     * Get the typeface for the given style and weight.
     *
     * @param style
     * The style of the typeface.
     * @param weight
     * The weight of the typeface.
     * @return The requested typeface, or the default typeface if it could not be found.
     */
    fun getTypeface(@Style style: Int, @IntRange(from = 100, to = 900) weight: Int): Typeface {
        return getTypeface(style, weight, defaultTypefaceWrapper.typeface)!!
    }

    /**
     * Get the typeface for the given style and weight.
     *
     * @param style
     * The style of the typeface.
     * @param weight
     * The weight of the typeface.
     * @param valueIfNotFound
     * The value to return if no matching typeface was found.
     * @return The requested typeface, or valueIfNotFound if it could not be found.
     */
    fun getTypeface(
        @Style style: Int,
        @IntRange(from = 100, to = 900) weight: Int,
        valueIfNotFound: Typeface?
    ): Typeface? {
        val faces = mTypefaces.get(style) ?: return valueIfNotFound

        val found = faces.get(weight) ?: return valueIfNotFound
        return found.typeface
    }

    /**
     * Get the typeface wrapper fot the given style and weight.
     *
     * @param style
     * The style of the typeface wrapper.
     * @param weight
     * The weight of the typeface wrapper.
     * @return The requested typeface wrapper, or the default typeface wrapper if it could not be
     * found.
     */
    fun getTypefaceWrapper(
        @Style style: Int,
        @IntRange(from = 100, to = 900) weight: Int
    ): TypefaceWrapper {
        return getTypefaceWrapper(style, weight, defaultTypefaceWrapper)!!
    }

    /**
     * Get the typeface wrapper for the given style and weight.
     *
     * @param style
     * The style of the typeface wrapper.
     * @param weight
     * The weight of the typeface wrapper.
     * @param valueIfNotFound
     * The value to return if no matching typeface wrapper was found.
     * @return The requested typeface wrapper, or valueIfNotFound if it could not be found.
     */
    fun getTypefaceWrapper(
        @Style style: Int,
        @IntRange(from = 100, to = 900) weight: Int,
        valueIfNotFound: TypefaceWrapper?
    ): TypefaceWrapper? {
        val faces = mTypefaces.get(style) ?: return valueIfNotFound

        return faces.get(weight) ?: return valueIfNotFound
    }

    /**
     * Get the wrapper that contains the given typeface.
     *
     * @param typeface
     * The typeface to search for.
     * @return The wrapper containing typeface, or `null` if the typeface was not found.
     */
    fun getWrappedTypeface(typeface: Typeface): TypefaceWrapper? {
        val style = if (typeface.isItalic) ITALIC else NORMAL
        val faces = mTypefaces.get(style)
        if (faces == null || faces.size() == 0) {
            return null
        }

        for (i in 0 until faces.size()) {
            val wrapper = faces.valueAt(i)
            if (typeface === wrapper.typeface) {
                return wrapper
            }
        }

        if (typeface === monospaceTypefaceWrapper.typeface) {
            return monospaceTypefaceWrapper
        }
        return if (typeface === defaultTypefaceWrapper.typeface) {
            defaultTypefaceWrapper
        } else null
    }

    /**
     * Get all wrapped typefaces for the given style.
     *
     * @param style
     * Style to get all typefaces for.
     * @return Array with all the typefaces for the given style.
     */
    fun getAllFaces(@Style style: Int): SparseArray<TypefaceWrapper> {
        var result: SparseArray<TypefaceWrapper>? = mTypefaces.get(style)
        if (result == null) {
            result = SparseArray(0)
        }
        return result
    }

    /**
     * Get the typeface with matching style that is closest to weight.
     *
     * @param style
     * Style of the typeface to get.
     * @param weight
     * What weight to use as target.
     * @return The typeface with the closest weight, or the default typeface if no typeface with the
     * given style exists in the family.
     */
    fun getClosestTypeface(
        @Style style: Int,
        @IntRange(from = 100, to = 900) weight: Int
    ): Typeface {
        return getClosestValue(mTypefaces.get(style), weight, defaultTypefaceWrapper)!!.typeface
    }

    companion object {
        private val TAG = "FontFamily"

        const val UNKNOWN = 0
        const val SANS_SERIF = 1
        const val SERIF = 2

        const val PROPORTIONAL = 0
        const val MONOSPACE = 1

        val defaultFontFamily: FontFamily

        init {
            defaultFontFamily = FontFamily(UNKNOWN, PROPORTIONAL)
            defaultFontFamily.addTypeface(Typeface.defaultFromStyle(Typeface.NORMAL))
            defaultFontFamily.addTypeface(Typeface.defaultFromStyle(Typeface.BOLD))
            defaultFontFamily.addTypeface(Typeface.defaultFromStyle(Typeface.ITALIC))
            defaultFontFamily.addTypeface(Typeface.defaultFromStyle(Typeface.BOLD_ITALIC))
        }
    }
}
