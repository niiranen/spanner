package net.niiranen.spanner.xml.handler

import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ImageSpan
import net.niiranen.spanner.SpanHandler
import org.xml.sax.Attributes

typealias ImageGetter = (source: String) -> Drawable

class ImageSpanHandler(private val imageGetter: ImageGetter) : SpanHandler<Attributes> {
    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        val source: String? = spanData.getValue("src")
        val alt: String? = spanData.getValue("alt")
        val start = text.length

        if (alt.isNullOrEmpty()) {
            text.append("\uFFFC")
        } else {
            text.append(alt)
        }

        if (source.isNullOrEmpty()) {
            return
        }
        val drawable = imageGetter(source)

        text.setSpan(ImageSpan(drawable), start, text.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    override fun endSpan(text: SpannableStringBuilder) {}
}
