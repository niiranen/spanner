package net.niiranen.spanner.sample

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.PermissionChecker
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.method.LinkMovementMethod
import android.util.Log
import android.widget.TextView
import net.niiranen.spanner.FontFamily
import net.niiranen.spanner.FontLoader
import net.niiranen.spanner.TypefaceWrapper
import net.niiranen.spanner.xml.XmlParser
import net.niiranen.spanner.xml.handler.ImageSpanHandler
import org.xml.sax.SAXException
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import javax.xml.parsers.ParserConfigurationException

class MainActivity : AppCompatActivity() {
    private val textView: TextView by lazy { findViewById<TextView>(R.id.text) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (PermissionChecker.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 0)
        } else {
            loadContent()
        }
    }

    private fun loadContent() {
        val ff = FontFamily()
        ff.addTypeface(TypefaceWrapper(
                Typeface.createFromAsset(assets, "alegreya/Alegreya-Black.ttf"),
                TypefaceWrapper.NORMAL, TypefaceWrapper.BLACK))
        ff.addTypeface(TypefaceWrapper(
                Typeface.createFromAsset(assets, "alegreya/Alegreya-BlackItalic.ttf"),
                TypefaceWrapper.ITALIC, TypefaceWrapper.BLACK))
        ff.addTypeface(TypefaceWrapper(
                Typeface.createFromAsset(assets, "alegreya/Alegreya-Bold.ttf"),
                TypefaceWrapper.NORMAL, TypefaceWrapper.BOLD))
        ff.addTypeface(TypefaceWrapper(
                Typeface.createFromAsset(assets, "alegreya/Alegreya-BoldItalic.ttf"),
                TypefaceWrapper.ITALIC, TypefaceWrapper.BOLD))
        ff.addTypeface(TypefaceWrapper(
                Typeface.createFromAsset(assets, "alegreya/Alegreya-Regular.ttf"),
                TypefaceWrapper.NORMAL, TypefaceWrapper.REGULAR))
        ff.addTypeface(TypefaceWrapper(
                Typeface.createFromAsset(assets, "alegreya/Alegreya-Italic.ttf"),
                TypefaceWrapper.ITALIC, TypefaceWrapper.REGULAR))
        ff.defaultTypeface = ff.getTypeface(TypefaceWrapper.NORMAL, TypefaceWrapper.REGULAR)

        try {
            val petitFormalScript = FontLoader.loadFontFamily(File(
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS),
                    "petitformalscript"))
            textView.typeface = petitFormalScript.defaultTypeface
        } catch (e: IOException) {
            Log.e("MainActivity", "Unable to load font", e)
        }

        var spannable: Spannable? = null
        try {
            val parser = XmlParser(ff, tagHandlers = XmlParser.defaultTagHandlers.plus("img" to ImageSpanHandler { source ->
                val image = assets.open(source)
                val options =  BitmapFactory.Options().apply {
                    inPreferredConfig = Bitmap.Config.RGB_565
                    inSampleSize = 8
                }
                BitmapDrawable(resources, BitmapFactory.decodeStream(image, null, options)).apply {
                    setBounds(0, 0, options.outWidth, options.outHeight)
                }
            }))
            spannable = parser.parse(InputStreamReader(resources.openRawResource(R.raw.teststring)))
        } catch (e: SAXException) {
            e.printStackTrace()
        } catch (e: ParserConfigurationException) {
            e.printStackTrace()
        }

        textView.text = spannable
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish()
            } else {
                loadContent()
            }
        }
    }
}
