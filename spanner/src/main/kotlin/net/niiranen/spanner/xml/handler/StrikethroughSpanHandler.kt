package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder
import android.text.style.StrikethroughSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class StrikethroughSpanHandler : SpanHandler<Attributes> {
    class StrikethroughSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, StrikethroughSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, StrikethroughSpanMark::class.java, StrikethroughSpan())
    }
}
