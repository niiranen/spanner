package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder
import android.text.style.SubscriptSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class SubscriptSpanHandler : SpanHandler<Attributes> {
    class SubscriptSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, SubscriptSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, SubscriptSpanMark::class.java, SubscriptSpan())
    }
}
