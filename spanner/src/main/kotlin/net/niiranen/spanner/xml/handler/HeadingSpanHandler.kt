package net.niiranen.spanner.xml.handler

import android.graphics.Typeface
import android.support.annotation.IntRange
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.getLastSpan
import net.niiranen.spanner.util.SpanUtil.markSpan

class HeadingSpanHandler(@param:IntRange(from = 1, to = 6) private val mRank: Int) : SpanHandler<Attributes> {
    class HeadingSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        sParagraphHandler.startSpan(text, spanData)
        markSpan(text, HeadingSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        val mark = getLastSpan(text, HeadingSpanMark::class.java) ?: return
        var end = text.length
        val start = text.getSpanStart(mark)
        text.removeSpan(mark)
        // Back off not to change only the text, not the blank line.
        while (end > start && text[end - 1] == '\n') {
            end--
        }
        if (start != end) {
            text.setSpan(RelativeSizeSpan(sRankSizes[mRank - 1]), start, end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            text.setSpan(StyleSpan(Typeface.BOLD), start, end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        sParagraphHandler.endSpan(text)
    }

    companion object {

        private val sRankSizes = floatArrayOf(1.5f, 1.4f, 1.3f, 1.2f, 1.1f, 1.0f)

        /** Used to wrap the heading in a paragraph  */
        private val sParagraphHandler = ParagraphSpanHandler()
    }
}
