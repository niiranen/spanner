package net.niiranen.spanner.xml.handler

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class QuoteSpanHandler : SpanHandler<Attributes> {

    class QuoteSpanMark(val cite: String)

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, QuoteSpanMark(spanData.getValue("cite")))
        text.append(LEFT_QUOTE)
    }

    override fun endSpan(text: SpannableStringBuilder) {
        text.append(RIGHT_QUOTE)
        replaceLastMark(text, QuoteSpanMark::class.java, StyleSpan(Typeface.ITALIC))
    }

    companion object {
        private val LEFT_QUOTE = "\u201C" // “
        private val RIGHT_QUOTE = "\u201D" // ”
    }
}
