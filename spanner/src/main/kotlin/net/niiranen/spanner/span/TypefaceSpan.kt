package net.niiranen.spanner.span

import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.MetricAffectingSpan

class TypefaceSpan : MetricAffectingSpan() {

    override fun updateMeasureState(p: TextPaint) {
    }

    override fun updateDrawState(tp: TextPaint) {
    }

    protected fun apply(tp: TextPaint, face: Typeface) {
        tp.typeface = face
    }
}
