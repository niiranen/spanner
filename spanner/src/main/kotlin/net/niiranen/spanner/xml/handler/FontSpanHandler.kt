package net.niiranen.spanner.xml.handler

import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.text.style.TextAppearanceSpan
import android.text.style.TypefaceSpan
import android.util.Log

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.getLastSpan
import net.niiranen.spanner.util.SpanUtil.markSpan

class FontSpanHandler : SpanHandler<Attributes> {

    class FontSpanMark(val color: String?, val face: String?, val size: String?)

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, FontSpanMark(spanData.getValue("color"), spanData.getValue("face"),
                spanData.getValue("size")))
    }

    override fun endSpan(text: SpannableStringBuilder) {
        val mark = getLastSpan(text, FontSpanMark::class.java)!!
        val start = text.getSpanStart(mark)
        text.removeSpan(mark)
        val end = text.length
        if (start != end) {
            if (!TextUtils.isEmpty(mark.color)) {
                if (mark.color!!.startsWith("@")) {
                    val res = Resources.getSystem()
                    val name = mark.color.substring(1)
                    @ColorRes val color = res.getIdentifier(name, "color", "android")
                    if (color != 0) {
                        val colors: ColorStateList
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            colors = res.getColorStateList(color, null)
                        } else {
                            @Suppress("DEPRECATION")
                            colors = res.getColorStateList(color)
                        }
                        text.setSpan(TextAppearanceSpan(null, 0, 0, colors, null), start, end,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                } else {
                    @ColorInt var color: Int
                    try {
                        color = Color.parseColor(mark.color)
                    } catch (e: IllegalArgumentException) {
                        Log.d(TAG, "Unable to find color " + mark.color, e)
                        color = Integer.getInteger(mark.color, Color.WHITE)!!
                    }

                    text.setSpan(ForegroundColorSpan(color), start, end,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                }
            }
            if (!TextUtils.isEmpty(mark.size)) {
                try {
                    text.setSpan(AbsoluteSizeSpan(Integer.parseInt(mark.size!!), true),
                            start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                } catch (e: NumberFormatException) {
                    Log.e(TAG, "Unable to parse font size", e)
                }
            }
            if (!TextUtils.isEmpty(mark.face)) {
                text.setSpan(TypefaceSpan(mark.face), start, end,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }

    companion object {
        private val TAG = "FontSpanHandler"
    }
}
