package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.style.URLSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.getLastSpan
import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceMark

class AnchorSpanHandler : SpanHandler<Attributes> {
    class AnchorSpanMark(val href: String)

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, AnchorSpanMark(spanData.getValue("href")))
    }

    override fun endSpan(text: SpannableStringBuilder) {
        val mark = getLastSpan(text, AnchorSpanMark::class.java)

        if (mark != null && !TextUtils.isEmpty(mark.href)) {
            replaceMark(text, mark, URLSpan(mark.href))
        } else {
            text.removeSpan(mark)
        }
    }
}
