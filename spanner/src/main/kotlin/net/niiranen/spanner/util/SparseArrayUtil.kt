package net.niiranen.spanner.util

import android.util.SparseArray

/**
 * Help methods for [SparseArray].
 */
object SparseArrayUtil {
    /**
     * Get the value of the key that is closest in value of the given key.
     *
     * @param array
     * The array to search in.
     * @param value
     * The value to search for.
     * @param valueIfNotFound
     * Value to return if no value was found.
     * @return The value closest to, or at key. valueIfNotFound is returned if array is `null`
     * or empty.
     */
    fun <T> getClosestValue(
        array: SparseArray<T>?,
        value: Int,
        valueIfNotFound: T?
    ): T? {
        if (array == null || array.size() == 0) {
            return valueIfNotFound
        }

        var closestDist: Int
        var closestIndex = array.indexOfKey(value)
        if (closestIndex >= 0) {
            return array.valueAt(closestIndex)
        }

        closestDist = Math.abs(array.keyAt(0) - value)
        closestIndex = 0
        for (i in 1 until array.size()) {
            val dist = Math.abs(array.keyAt(i) - value)
            if (dist < closestDist) {
                closestDist = dist
                closestIndex = i
            }
        }
        return array.valueAt(closestIndex)
    }

    /**
     * Get the value of the smallest key that is larger than the given value.
     *
     * @param array
     * The array to search in.
     * @param value
     * The value to search for.
     * @param valueIfNotFound
     * The value to return if no value was found.
     * @return The value of the next larger key, or valueIfNotFound if no value was found.
     */
    fun <T> getValueOfSmallestLargerKey(
        array: SparseArray<T>?,
        value: Int,
        valueIfNotFound: T?
    ): T? {
        if (array == null || array.size() == 0) {
            return valueIfNotFound
        }

        // Use Long.MAX_VALUE to ensure that there is no collision with any key.
        var largestKey = java.lang.Long.MAX_VALUE
        var nextIndex = -1
        for (i in 0 until array.size()) {
            val k = array.keyAt(i)
            if (k <= value) {
                continue
            }

            if (k < largestKey) {
                largestKey = k.toLong()
                nextIndex = i
            }
        }
        return if (nextIndex >= 0) {
            array.valueAt(nextIndex)
        } else valueIfNotFound
    }
}
