package net.niiranen.spanner

import android.graphics.Typeface
import android.support.annotation.IntDef
import android.support.annotation.IntRange

import kotlin.annotation.Retention

/**
 * Wraps the given typeface, style and weight will be calculated from typeface.
 */
class TypefaceWrapper(
    typeface: Typeface,
    @Style style: Int = if (typeface.isItalic) ITALIC else NORMAL,
    @IntRange(from = 100, to = 900) weight: Int = if (typeface.isBold) BOLD else REGULAR
) {

    /**
     * Get the wrapped [Typeface].
     */
    val typeface: Typeface = typeface

    /**
     * Get the style of the typeface.
     *
     * @return The style of the typeface.
     */
    @Style
    val style: Int = style

    /**
     * Get the weight of the typeface.
     *
     * @return The weight of the typeface. In the range of `[100..900]`.
     */
    @IntRange(from = 100, to = 900)
    val weight: Int = weight

    /**
     * Check if the style is [ITALIC].
     *
     * @return True if the style is [ITALIC], otherwise false.
     */
    val isItalic: Boolean
        get() = style == ITALIC

    /**
     * Check if the typeface weight is bold or heavier.
     *
     * @return True if the weight is greater or equal to 600, otherwise false.
     */
    val isBold: Boolean
        get() = weight >= BOLD

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(NORMAL, ITALIC)
    annotation class Style

    companion object {
        /** Represents a normal text style.  */
        const val NORMAL = 0
        /** Represents a italic text style.  */
        const val ITALIC = 1

        /** A weight of 100  */
        const val THIN = 100
        /** A weight of 200  */
        const val EXTRA_LIGHT = 200
        /** A weight of 300  */
        const val LIGHT = 300
        /** A weight of 400  */
        const val REGULAR = 400
        /** A weight of 500  */
        const val MEDIUM = 500
        /** A weight of 600  */
        const val SEMI_BOLD = 600
        /** A weight of 700  */
        const val BOLD = 700
        /** A weight of 800  */
        const val HEAVY = 800
        /** A weight of 900  */
        const val BLACK = 900
    }
}
