package net.niiranen.spanner.xml.handler

import android.support.annotation.FloatRange
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class RelativeSizeSpanHandler(@param:FloatRange(from = 0.0) private val mMultiplier: Float) : SpanHandler<Attributes> {
    class RelativeSizeSpanMarker

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, RelativeSizeSpanMarker())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, RelativeSizeSpanMarker::class.java, RelativeSizeSpan(mMultiplier))
    }
}
