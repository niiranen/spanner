package net.niiranen.spanner

import android.content.res.AssetManager
import android.graphics.Typeface
import android.support.annotation.WorkerThread
import android.util.JsonReader
import android.util.Log

import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.URL
import java.util.HashMap

object FontLoader {
    private val TAG = "FontLoader"

    /** Cache of loaded typefaces.  */
    private val sCachedTypefaces: MutableMap<String, Typeface?> = HashMap()

    /**
     * Load a typeface from the asset files.
     *
     * @param mgr
     * [AssetManager] to use.
     * @param path
     * Where the typeface is.
     * @return The loaded typeface, or `null`.
     */
    fun loadTypeface(
        mgr: AssetManager,
        path: String
    ): Typeface? {
        synchronized(sCachedTypefaces) {
            if (!sCachedTypefaces.containsKey(path)) {
                try {
                    val typeface = Typeface.createFromAsset(mgr, path)
                    sCachedTypefaces[path] = typeface
                    return typeface
                } catch (e: Exception) {
                    Log.e(TAG, "Unable to load font", e)
                    sCachedTypefaces[path] = null
                    return null
                }
            }
            return sCachedTypefaces[path]
        }
    }

    /**
     * Load a typeface from the local filesystem.
     *
     * @param path
     * Where the typeface is.
     * @return The loaded typeface, or `null`.
     */
    fun loadTypeface(path: File): Typeface? {
        return loadTypeface(path.absolutePath)
    }

    /**
     * Load a typeface from the local filesystem.
     *
     * @param path
     * Where the typeface is.
     * @return The loaded typeface, or `null`.
     */
    fun loadTypeface(path: String): Typeface? {
        synchronized(sCachedTypefaces) {
            if (!sCachedTypefaces.containsKey(path)) {
                try {
                    val typeface = Typeface.createFromFile(path)
                    sCachedTypefaces[path] = typeface
                    return typeface
                } catch (e: RuntimeException) {
                    Log.e(TAG, "Unable to load font", e)
                    sCachedTypefaces[path] = null
                    return null
                }
            }
            return sCachedTypefaces[path]
        }
    }

    /**
     * Download and load a [Typeface].
     *
     * @param path
     * Where to download from.
     * @param downloadLocation
     * Where to download to.
     * @return The typeface that was downloaded, or `null`.
     * @throws IOException
     * If downloading failed.
     */
    @WorkerThread
    @Throws(IOException::class)
    fun loadTypeface(path: URL, downloadLocation: File): Typeface? {
        val key = path.toString()
        synchronized(sCachedTypefaces) {
            if (sCachedTypefaces.containsKey(key)) {
                return sCachedTypefaces[key]
            }
        }

        val connection = path.openConnection()
        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null
        try {
            inputStream = BufferedInputStream(connection.getInputStream())
            outputStream = FileOutputStream(downloadLocation)

            val buffer = ByteArray(4096)
            var read: Int
            do {
                read = inputStream.read(buffer)
                if (read >= 0) {
                    outputStream.write(buffer, 0, read)
                }
            } while (read >= 0)
            outputStream.flush()
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (ignored: Exception) {
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close()
                } catch (ignored: Exception) {
                }
            }
        }

        var typeface: Typeface? = null
        try {
            typeface = Typeface.createFromFile(downloadLocation.absolutePath)
        } catch (e: RuntimeException) {
            Log.e(TAG, "Unable to load font", e)
        }

        synchronized(sCachedTypefaces) {
            // Make sure it has not been added while we downloaded
            if (sCachedTypefaces.containsKey(key)) {
                return sCachedTypefaces[key]
            }
            sCachedTypefaces[key] = typeface!!
            return typeface
        }
    }

    /**
     * Loads a font family from directory, format is assumed to be in the same as [google fonts](https://github.com/google/fonts).
     *
     * @param directory
     * The directory of the font family.
     * @return The new font family.
     * @throws IOException
     * If the json parsing failed.
     */
    @Throws(IOException::class)
    fun loadFontFamily(directory: File): FontFamily {
        val reader = JsonReader(FileReader(File(directory, "METADATA.json")))
        val metadata = FontFamilyMetadata.fromJson(reader)
        reader.close()

        @FontFamily.SerifStyle var category = FontFamily.UNKNOWN
        val metadataCategory = metadata.category
        if (metadataCategory!!.equals("sans serif", ignoreCase = true)) {
            category = FontFamily.SANS_SERIF
        } else if (metadataCategory.equals("serif", ignoreCase = true)) {
            category = FontFamily.SERIF
        }
        @FontFamily.Proportion var proportion = FontFamily.PROPORTIONAL
        if (metadataCategory.equals("monospace", ignoreCase = true)) {
            proportion = FontFamily.MONOSPACE
        }

        val fontFamily = FontFamily(category, proportion)

        val fonts = metadata.fontInfos
        for (font in fonts!!) {
            val typeface = loadTypeface(File(directory, font.filename)) ?: continue
            @TypefaceWrapper.Style var style = TypefaceWrapper.NORMAL
            if (font.style!!.equals("italic", ignoreCase = true)) {
                style = TypefaceWrapper.ITALIC
            }
            fontFamily.addTypeface(TypefaceWrapper(typeface, style, font.weight))
            if (style == TypefaceWrapper.NORMAL && font.weight == TypefaceWrapper.REGULAR) {
                if (proportion == FontFamily.MONOSPACE) {
                    fontFamily.monospaceTypeface = typeface
                }
                fontFamily.defaultTypeface = typeface
            }
        }

        return fontFamily
    }
}
