package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder
import android.text.style.SuperscriptSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class SuperscriptSpanHandler : SpanHandler<Attributes> {
    class SuperscriptSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, SuperscriptSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, SuperscriptSpanMark::class.java, SuperscriptSpan())
    }
}
