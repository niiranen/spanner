package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder

import net.niiranen.spanner.SpanHandler
import net.niiranen.spanner.span.BlockQuoteSpan

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class BlockQuoteSpanHandler : SpanHandler<Attributes> {
    class BlockQuoteMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        sParagraphHandler.startSpan(text, spanData)
        markSpan(text, BlockQuoteMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, BlockQuoteMark::class.java, BlockQuoteSpan())
        sParagraphHandler.endSpan(text)
    }

    companion object {

        /** Used to wrap the block quote in a paragraph  */
        private val sParagraphHandler = ParagraphSpanHandler()
    }
}
