package net.niiranen.spanner.span

import android.graphics.Paint
import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.MetricAffectingSpan

import net.niiranen.spanner.FontFamily

import net.niiranen.spanner.TypefaceWrapper.Companion.BOLD
import net.niiranen.spanner.TypefaceWrapper.Companion.ITALIC
import net.niiranen.spanner.TypefaceWrapper.Companion.NORMAL
import net.niiranen.spanner.TypefaceWrapper.Companion.REGULAR

class FontFamilySpan(override val fontFamily: FontFamily) : MetricAffectingSpan(), FontFamilyContainingSpan {
    private val default: Typeface = fontFamily.defaultTypeface

    class FontFamilySpanMark(override val fontFamily: FontFamily) : FontFamilyContainingSpan

    override fun updateMeasureState(p: TextPaint) {
        setTypeface(p)
    }

    override fun updateDrawState(tp: TextPaint) {
        setTypeface(tp)
    }

    private fun setTypeface(p: Paint) {
        var typeface = default
        val oldTypeface: Typeface? = p.typeface
        val oldStyle = oldTypeface?.style ?: Typeface.NORMAL
        val missingStyle = oldStyle and typeface.style.inv()

        if (missingStyle != 0) {
            assert(oldTypeface != null)
            val override = fontFamily.getTypeface(if (oldTypeface?.isItalic == true) ITALIC else NORMAL,
                    if (oldTypeface?.isBold == true) BOLD else REGULAR, null)
            if (override != null) {
                typeface = override
            } else {
                if (missingStyle and Typeface.BOLD != 0) {
                    p.isFakeBoldText = true
                }
                if (missingStyle and Typeface.ITALIC != 0) {
                    p.textSkewX = -0.25f
                }
            }
        }

        p.typeface = typeface
    }
}
