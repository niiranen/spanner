package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

class BreakLineSpanHandler : SpanHandler<Attributes> {
    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        // No-op, only add new line when span is closed.
    }

    override fun endSpan(text: SpannableStringBuilder) {
        text.append(System.lineSeparator())
    }
}
