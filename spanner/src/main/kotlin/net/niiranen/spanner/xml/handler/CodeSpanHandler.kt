package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder
import android.text.style.TypefaceSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class CodeSpanHandler : SpanHandler<Attributes> {
    class CodeSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, CodeSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, CodeSpanMark::class.java, TypefaceSpan("monospace"))
    }
}
