package net.niiranen.spanner.span

import android.support.annotation.IntRange
import android.text.TextPaint
import android.text.style.MetricAffectingSpan

class HeadingSpan(@IntRange(from = 1, to = 6) rank: Int) : MetricAffectingSpan() {

    private val mRank: Int

    init {
        mRank = rank - 1
    }

    override fun updateMeasureState(p: TextPaint) {
        p.textSize = p.textSize * sRankSizes[mRank]
    }

    override fun updateDrawState(tp: TextPaint) {
        tp.textSize = tp.textSize * sRankSizes[mRank]
    }

    companion object {
        private val sRankSizes = floatArrayOf(1.5f, 1.4f, 1.3f, 1.2f, 1.1f, 1.0f)
    }
}
