package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder
import android.text.style.UnderlineSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class UnderlineSpanHandler : SpanHandler<Attributes> {
    class UnderlineSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, UnderlineSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, UnderlineSpanMark::class.java, UnderlineSpan())
    }
}
