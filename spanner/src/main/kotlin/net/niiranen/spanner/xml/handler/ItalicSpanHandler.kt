package net.niiranen.spanner.xml.handler

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

import net.niiranen.spanner.util.SpanUtil.markSpan
import net.niiranen.spanner.util.SpanUtil.replaceLastMark

class ItalicSpanHandler : SpanHandler<Attributes> {
    class ItalicSpanMark

    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        markSpan(text, ItalicSpanMark())
    }

    override fun endSpan(text: SpannableStringBuilder) {
        replaceLastMark(text, ItalicSpanMark::class.java, StyleSpan(Typeface.ITALIC))
    }
}
