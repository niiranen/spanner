package net.niiranen.spanner

import android.text.Spannable

import java.io.Reader

/**
 * Interface for span parsers.
 *
 * @param <SpanIdType>
 * Type for span id.
 * @param <SpanDataType>
 * Type for span data.
</SpanDataType></SpanIdType> */
interface Parser<SpanIdType, SpanDataType> {
    /**
     * Start a spanned text.
     */
    fun startText()

    /**
     * End a spanned text.
     */
    fun endText()

    /**
     * Start a span.
     * @param spanId Id of the span to start.
     * @param spanData Data for the span to start.
     */
    fun startSpan(spanId: SpanIdType, spanData: SpanDataType)

    /**
     * End a span.
     * @param spanId Id of the span to end.
     */
    fun endSpan(spanId: SpanIdType)

    /**
     * Get a [Spannable] from a [Reader].
     * @param reader Reader that contains span data.
     * @return
     */
    fun parse(reader: Reader): Spannable
}
