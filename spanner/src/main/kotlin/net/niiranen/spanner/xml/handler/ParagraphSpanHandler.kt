package net.niiranen.spanner.xml.handler

import android.text.SpannableStringBuilder

import net.niiranen.spanner.SpanHandler

import org.xml.sax.Attributes

class ParagraphSpanHandler : SpanHandler<Attributes> {
    override fun startSpan(text: SpannableStringBuilder, spanData: Attributes) {
        addWhitespace(text)
    }

    override fun endSpan(text: SpannableStringBuilder) {
        addWhitespace(text)
    }

    internal fun addWhitespace(text: SpannableStringBuilder) {
        val len = text.length

        if (len >= 1 && text[len - 1] == '\n') {
            if (len >= 2 && text[len - 2] == '\n') {
                return
            }

            text.append("\n")
            return
        }

        if (len != 0) {
            text.append("\n\n")
        }
    }
}
