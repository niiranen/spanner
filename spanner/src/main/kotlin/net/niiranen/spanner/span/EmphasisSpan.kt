package net.niiranen.spanner.span

import android.graphics.Paint
import android.text.TextPaint
import android.text.style.MetricAffectingSpan

import net.niiranen.spanner.FontFamily
import net.niiranen.spanner.TypefaceWrapper

import net.niiranen.spanner.util.SparseArrayUtil.getValueOfSmallestLargerKey

class EmphasisSpan(private val mFontFamily: FontFamily, private val mParents: Int) : MetricAffectingSpan() {

    override fun updateMeasureState(p: TextPaint) {
        apply(p)
    }

    override fun updateDrawState(tp: TextPaint) {
        apply(tp)
    }

    fun apply(p: Paint) {
        val oldTypeface = p.typeface
        var wrapper = mFontFamily.getWrappedTypeface(oldTypeface)

        if (wrapper == null) {
            wrapper = TypefaceWrapper(oldTypeface)
        }

        // Alternate between italic and normal styles.
        val style = if (mParents and 1 == 0) TypefaceWrapper.ITALIC else TypefaceWrapper.NORMAL

        val currentWeight = wrapper.weight
        val faces = mFontFamily.getAllFaces(style)

        var newWrapper: TypefaceWrapper?
        if (style == TypefaceWrapper.NORMAL) {
            // Try to fallback to a normal style typeface with matching weight.
            val fallback = faces.get(currentWeight, wrapper)
            // If the style is normal, we should get a heavier typeface.
            newWrapper = getValueOfSmallestLargerKey(faces, currentWeight, fallback)
        } else {
            newWrapper = faces.get(currentWeight)
            if (newWrapper == null || wrapper === newWrapper) {
                // If the italic style does not contain the requested weight, get a heavier one.
                newWrapper = getValueOfSmallestLargerKey(faces, currentWeight, wrapper)
            }
        }

        if (wrapper === newWrapper) {
            // No alternative typeface found, fake the typeface
            if (style == TypefaceWrapper.NORMAL) {
                p.isFakeBoldText = true
                if (p.textSkewX == -0.25f) {
                    p.textSkewX = 0f
                }
            } else {
                if (!wrapper.isItalic && p.textSkewX == 0f) {
                    p.textSkewX = -0.25f
                }
            }
            return
        }
        p.typeface = newWrapper!!.typeface
    }
}
