package net.niiranen.spanner.util

import android.text.Spannable
import android.text.Spanned

object SpanUtil {
    /**
     * Get the last span of kind in text.
     *
     * @param text
     * The [Spanned] to get the last span from.
     * @param kind
     * What kind of span to get.
     * @return The last span of kind, or `null` if there was no spans of kind.
     */
    fun <T> getLastSpan(text: Spanned, kind: Class<T>): T? {
        val spans = text.getSpans(0, text.length, kind)

        return if (spans.size == 0) {
            null
        } else {
            spans[spans.size - 1]
        }
    }

    /**
     * Get the first parent of kind.
     *
     * @param text
     * The [Spanned] to get the parent from.
     * @param kind
     * What kind of parent to get.
     * @return The first parent of kind, or `null` if no parent of kind was found.
     */
    fun <T> getParentSpan(text: Spanned, kind: Class<T>): T? {
        val spans = text.getSpans(0, text.length, kind)

        return if (spans.size <= 1) {
            null
        } else {
            spans[spans.size - 2]
        }
    }

    /**
     * Count the parents of kind.
     *
     * @param text
     * The [Spanned] to get the parent from.
     * @param kind
     * What kind of parent to count.
     * @return The first parent of kind, or `null` if no parent of kind was found.
     */
    fun <T> countParentSpans(text: Spanned, kind: Class<T>): Int {
        val spans = text.getSpans(0, text.length, kind)

        return if (spans.size <= 1) {
            0
        } else {
            spans.size - 1
        }
    }

    /**
     * Add a span marker to a [Spannable]. The marker will be placed at the end of text.
     *
     * @param text
     * The [Spannable] to add the marker to.
     * @param mark
     * The mark to add.
     */
    fun markSpan(text: Spannable, mark: Any) {
        val location = text.length
        text.setSpan(mark, location, location, Spanned.SPAN_MARK_MARK)
    }

    /**
     * Replace a marker with a real span.
     *
     *
     * Note: The start of the span will be the location of the mark. The end will be at the end of
     * text.
     *
     * @param text
     * The text to replace the mark in.
     * @param markKind
     * What kind of mark to replace.
     * @param span
     * The span object that should replace the mark.
     */
    fun replaceLastMark(
        text: Spannable,
        markKind: Class<out Any>,
        span: Any
    ) {
        val mark = getLastSpan(text, markKind)
        if (mark != null) {
            replaceMark(text, mark, span)
        }
    }

    fun replaceMark(
        text: Spannable,
        mark: Any,
        span: Any
    ) {
        val start = text.getSpanStart(mark)
        text.removeSpan(mark)
        val end = text.length

        if (start != end) {
            text.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}
