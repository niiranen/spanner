package net.niiranen.spanner.xml

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.util.Log
import net.niiranen.spanner.FontFamily
import net.niiranen.spanner.Parser
import net.niiranen.spanner.SpanHandler
import net.niiranen.spanner.span.FontFamilySpan
import net.niiranen.spanner.util.SpanUtil
import net.niiranen.spanner.xml.handler.AnchorSpanHandler
import net.niiranen.spanner.xml.handler.BlockQuoteSpanHandler
import net.niiranen.spanner.xml.handler.BreakLineSpanHandler
import net.niiranen.spanner.xml.handler.CodeSpanHandler
import net.niiranen.spanner.xml.handler.EmphasisSpanHandler
import net.niiranen.spanner.xml.handler.FontSpanHandler
import net.niiranen.spanner.xml.handler.HeadingSpanHandler
import net.niiranen.spanner.xml.handler.ImageGetter
import net.niiranen.spanner.xml.handler.ImageSpanHandler
import net.niiranen.spanner.xml.handler.ItalicSpanHandler
import net.niiranen.spanner.xml.handler.ParagraphSpanHandler
import net.niiranen.spanner.xml.handler.QuoteSpanHandler
import net.niiranen.spanner.xml.handler.RelativeSizeSpanHandler
import net.niiranen.spanner.xml.handler.StrikethroughSpanHandler
import net.niiranen.spanner.xml.handler.StrongSpanHandler
import net.niiranen.spanner.xml.handler.SubscriptSpanHandler
import net.niiranen.spanner.xml.handler.SuperscriptSpanHandler
import net.niiranen.spanner.xml.handler.UnderlineSpanHandler
import org.xml.sax.Attributes
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import org.xml.sax.XMLReader
import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.helpers.XMLReaderFactory
import java.io.IOException
import java.io.Reader

class XmlParser(
    fontFamily: FontFamily? = null,
    xmlReader: XMLReader? = null,
    tagHandlers: Map<String, SpanHandler<Attributes>>? = null
) : DefaultHandler(), Parser<String, Attributes> {
    private val fontFamily = fontFamily ?: FontFamily.defaultFontFamily
    private val xmlReader = xmlReader ?: defaultXMLReader.apply { contentHandler = this@XmlParser }
    private val tagHandlers = tagHandlers ?: defaultTagHandlers
    private var spannableStringBuilder: SpannableStringBuilder? = null

    /**
     * Get the default XMLReader.
     *
     *
     * Note: The TagSoup parser is used by [android.text.Html.fromHtml], so it safe to
     * load.
     */
    private val defaultXMLReader: XMLReader
        @Throws(SAXException::class)
        get() = XMLReaderFactory.createXMLReader("org.ccil.cowan.tagsoup.Parser")

    @Throws(SAXException::class)
    override fun startDocument() {
        startText()
    }

    @Throws(SAXException::class)
    override fun endDocument() {
        endText()
    }

    @Throws(SAXException::class)
    override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes) {
        startSpan(localName, attributes)
    }

    @Throws(SAXException::class)
    override fun endElement(uri: String, localName: String, qName: String) {
        endSpan(localName)
    }

    override fun startText() {
        spannableStringBuilder = SpannableStringBuilder()
        SpanUtil.markSpan(spannableStringBuilder!!, FontFamilySpan.FontFamilySpanMark(fontFamily))
        spannableStringBuilder!!.setSpan(FontFamilySpan(fontFamily),
                0, spannableStringBuilder!!.length,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE)
    }

    override fun endText() {}

    override fun startSpan(spanId: String, spanData: Attributes) {
        val handler = tagHandlers[spanId]
        handler?.startSpan(spannableStringBuilder!!, spanData)
    }

    override fun endSpan(spanId: String) {
        val handler = tagHandlers[spanId]
        handler?.endSpan(spannableStringBuilder!!)
    }

    override fun parse(reader: Reader): Spannable {
        try {
            xmlReader.parse(InputSource(reader))
        } catch (e: IOException) {
            Log.e(TAG, "Error parsing data", e)
        } catch (e: SAXException) {
            Log.e(TAG, "Error parsing data", e)
        }

        return spannableStringBuilder ?: SpannableStringBuilder()
    }

    @Throws(SAXException::class)
    override fun characters(ch: CharArray, start: Int, length: Int) {
        val sb = StringBuilder()

        /*
         * Ignore whitespace that immediately follows other whitespace;
         * newlines count as spaces.
         */
        for (i in 0 until length) {
            val c = ch[i + start]

            if (c == ' ' || c == '\n') {
                val pred: Char
                var len = sb.length

                if (len == 0) {
                    len = spannableStringBuilder!!.length

                    pred = if (len == 0) {
                        '\n'
                    } else {
                        spannableStringBuilder!![len - 1]
                    }
                } else {
                    pred = sb[len - 1]
                }

                if (pred != ' ' && pred != '\n') {
                    sb.append(' ')
                }
            } else {
                sb.append(c)
            }
        }

        spannableStringBuilder!!.append(sb)
    }

    companion object {
        private const val TAG = "XmlParser"

        val defaultTagHandlers: Map<String, SpanHandler<Attributes>> by lazy {
            hashMapOf(
                    "br" to BreakLineSpanHandler(),
                    "p" to ParagraphSpanHandler(),
                    "div" to ParagraphSpanHandler(),
                    "strong" to StrongSpanHandler(),
                    "b" to StrongSpanHandler(),
                    "em" to EmphasisSpanHandler(),
                    "cite" to ItalicSpanHandler(),
                    "dfn" to ItalicSpanHandler(),
                    "i" to ItalicSpanHandler(),
                    "big" to RelativeSizeSpanHandler(1.2f),
                    "small" to RelativeSizeSpanHandler(0.8f),
                    "font" to FontSpanHandler(),
                    "q" to QuoteSpanHandler(),
                    "blockquote" to BlockQuoteSpanHandler(),
                    "tt" to CodeSpanHandler(),
                    "code" to CodeSpanHandler(),
                    "samp" to CodeSpanHandler(),
                    "a" to AnchorSpanHandler(),
                    "s" to StrikethroughSpanHandler(),
                    "u" to UnderlineSpanHandler(),
                    "sup" to SuperscriptSpanHandler(),
                    "sub" to SubscriptSpanHandler(),
                    "h1" to HeadingSpanHandler(1),
                    "h2" to HeadingSpanHandler(2),
                    "h3" to HeadingSpanHandler(3),
                    "h4" to HeadingSpanHandler(4),
                    "h5" to HeadingSpanHandler(5),
                    "h6" to HeadingSpanHandler(6),
                    "img" to ImageSpanHandler {
                        ColorDrawable(Color.CYAN).apply { setBounds(0, 0, 32, 32) }
                    }
            )
        }
    }
}
